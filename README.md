# Vue.js

Vue es un __framework__ open source de JavaScript para la construcción de interfaces de usuario.  diferencia de otros marcos 
monolíticos, Vue está diseñado desde cero para ser incrementalmente adoptable. La biblioteca principal 
se centra solo en la capa de vista y es fácil de recoger e integrar con otras bibliotecas o proyectos 
existentes. Por otro lado, Vue también es perfectamente capaz de impulsar aplicaciones sofisticadas de 
una sola página cuando se usa en combinación con herramientas modernas y bibliotecas de soporte.

La forma más fácil de probar Vue.js es utilizando su __cdn__, puede crear un archivo _index.html_ 
e incluir Vue con:

```html
<!-- production version, optimized for size and speed -->
<script src="https://cdn.jsdelivr.net/npm/vue"></script>
```
>Ejemplo [vue con cdn](https://codesandbox.io/s/github/vuejs/vuejs.org/tree/master/src/v2/examples/vue-20-hello-world).

La característica de ser __progresivo__ permite la adaptabilidad de vue, incluso con otros lenguajes de
programación, esto es posible gracias a su cdn, insertando pequeñas funcionalidades o script que
realicen ciertas tareas en medio de un proyecto PHP por ejemplo. Pero también podemos tener un 
proyecto completo y robusto con solo vue.

Una de las características que diferencian a Vue es su discreto sistema de reactividad. Los modelos 
simplemente son objetos de JavaScript. Cuando los modifique, se actualizará la vista. Esto hace que 
el gestor de estados sea simple e intuitivo, pero también es importante entender como funciona para 
prevenir algunos errores comunes. En esta sección, vamos a indagar en algunos detalles de bajo nivel 
del sistema de reactividad de Vue.

### Componentes
Ejemplo simple de un componente en Vue:
```javascript
// Definir un nuevo componente llamado ButtonContainer
Vue.component('ButtonContainer', {
  data: function () {
    return {
      count: 0
    }
  },
  template: '<button v-on:click="count++">Me ha pulsado {{ count }} veces.</button>'
})
```
Los componentes son instancias reutilizables de Vue con un nombre: en este caso, <ButtonContainer>. 
Podemos usar este componente como un elemento personalizado dentro de una instancia de Vue raíz 
creada con new Vue:

```html
<div id="components-demo">
  <ButtonContainer></ButtonContainer>
</div>
```

### Archivos .vue
Los componentes den vue tiene pueden ser organizados en archivos .vue e importarlos para ser 
reutilizados en diferentes compoenentes. Estos componentes tiene un estructura como la giguiebte:
```html
<template>
    <button v-on:click="count++">Me ha pulsado {{ count }} veces.</button>
</template>

<script>
    // exportacion del componente
    export default {
        name: 'ButtonComponent', // nombre del cmponenete 
        props:{
            // Propiedades que recibe el compoenete
        },
        data() {
            return {
                count: 0
            }
        },
        methods: {
           // finciones o metodos a ser ejecutados por evento
        }
    }
</script>

<style scoped>
    <!-- Estilos de la pagina   -->
</style>
```

## Ciclo de vida
Todo componente tiene un ciclo de vida con diferentes estados por los que acaba pasando. Muchos de los frameworks y librerías orientados a componentes nos dan la posibilidad  de incluir funciones en los diferentes estados por los que pasa un componente.

En el caso de VueJS existen 4 estados posibles. El framework nos va a permitir incluir acciones antes y después de que un componente se encuentre en un estado determinado. Estas acciones, conocidas como hooks, tienen varios propósitos para el desarrollador:


Lo primero que nos van a permitir es conocer el mecanismo interno de cómo se crea, actualiza y destruye un componente dentro de nuestro DOM. Esto nos ayuda a entender mejor la herramienta.

Lo segundo que nos aporta es la posibilidad de incluir trazas en dichas fases, lo que puede ser muy cómodo para aprender al principio cuando somos novatos en una herramienta y no sabemos como se va a comportar el sistema, por tanto, es un buen sistema para trazar componentes.

Lo tercero, y ultimo, es la posibilidad de incluir acciones que se tienen que dar antes o después de haber llegado a un estado interno del componente.

## Creando el componente

Un componente cuenta con un estado de creación. Este estado se produce entre la instanciación y el montaje del elemento en el DOM. Cuenta con dos hooks. Estos dos hooks son los únicos que pueden interceptarse en renderizado en servidor, debido su naturaleza, solo pueden ser usados en el navegador.


### beforeCreate
Este hook se realiza nada más instanciar un componente. Durante este hook no tiene sentido acceder al estado del componente pues todavía no se han registrado los observadores de los datos, ni se han registrado los eventos.

Aunque pueda parecer poco útil, utilizar este hook puede ser es un buen momento para dos acciones en particular:
- Para configurar ciertos parámetros internos u opciones, inherentes a las propias funcionalidad de VueJS. 
- Para iniciar librerías o estados externos.

### created

Cuando se ejecuta este hook, el componente acaba de registrar tanto los observadores como los eventos, pero todavía no ha sido ni renderizado ni incluido en el DOM. Por tanto, tenemos que tener en cuenta que dentro de created no podemos acceder a $el porque todavía no ha sido montado.

Es uno de los más usados y nos viene muy bien para iniciar variables del estado de manera asíncrona. Por ejemplo, necesitamos que un componente pinte los datos de un servicio determinado. Podríamos hacer algo como esto:

```
const component = {
    created: function () { 
        axios.get('/tasks') 
            .then(response => this.tasks = response.data) 
            .catch(error => this.errors.push(error));
    }
};
```

## Montando el componente

Una vez que el componente se ha creado, podemos entrar en una fase de montaje, es decir que se renderizará e insertará en el DOM. Puede darse el caso que al instanciar un componente no hayamos indicado la opción el. De ser así, el componente se encontraría en estado creado de manera latente hasta que se indique o hasta que ejecutemos el método $mount que lo que provocará es que el componente se renderice, pero no se monte (el montaje sería manual).


### beforeMount
Se ejecuta justo antes de insertar el componente en el DOM, justamente, en tiempo de la primera renderización de un componente. Es uno de los hooks que menos usarás y, como muchos otros, se podrá utilizar para trazar el ciclo de vida del componente.

A veces se usa para iniciar variables, pero yo te recomiendo que delegues esto al hook created.

### mounted
Es el hook que se ejecuta nada más renderizar e incluir el componente en el DOM. Nos puede ser muy útil para inicializar librerías externas. Imagínate que estás haciendo uso, dentro de un componente de VueJS, de un plugin de jQuery. Puede ser buen momento para ejecutar e iniciarlo en este punto, justamente cuando acabamos de incluirlo al DOM, es un hook que nos permite manipular el DOM nada más iniciarlo.

## Actualizando el componente
Cuando un componente ha sido creado y montado se encuentra a disposición del usuario. Cuando un componente entra en interacción con el usuario pueden darse eventos y cambios de estados. Estos cambios desembocan la necesidad de tener que volver a renderizar e incluir las diferencias provocadas dentro del DOM de nuevo. Es por eso, que el componente entra en un estado de actualización que también cuenta con dos hooks.

### beforeUpdate
Es el hook que se desencadena nada más que se provoca un actualización de estado, antes de que se se comience con el re renderizado del Virtual DOM y su posterior ‘mapeo’ en el DOM real.

Este hook es un buen sitio para trazar cuándo se provocan cambios de estado y se desembocan renderizados que nosotros no preveíamos o que son muy poco intuitivos a simple vista. Podríamos hacer lo siguiente:

```
const component = {
    beforeUpdate: function () {
        console.log('Empieza un nuevo renderizado de component');
    }
};
```

### updated
Se ejecuta una vez que el componente ha re renderizado los cambios en el DOM real. Al igual que ocurría con el hook mounted es buen momento para hacer ciertas manipulaciones del DOM externas a VueJS o hacer comprobaciones del estado de las variables en ese momento.

## Destruyendo el componente
Un componente puede ser destruido una vez que ya no es necesario para el usuario. Esta fase de desencadena cuando queremos eliminarlo del DOM y destruir la instancia de memoria.

### beforeDestroy
Se produce justamente antes de eliminar la instancia. El componente es totalmente operativo todavía y podemos acceder tanto al estado interno, como a sus propiedades y eventos.

### destroyed
Tanto los hijos internos, como las directivas, como sus eventos y escuchadores han sido eliminados. Este hook se ejecuta cuando la instancia ha sido eliminada. Nos puede ser muy útil para limpiar estados globales de nuestra aplicación.

## Qué son los props y cómo se crean
Los props dentro del componente en el que se declaran no son más que variables. Como pasa con las variables, los props también son reactivos, es decir, cuando desde fuera el valor del prop cambie, automáticamente se actualizará la vista y las propiedades computadas asociadas a ese prop.

### Variables computadas en Vue

Los props los tienes que declarar dentro de la sección props del componente. A diferencia de las variables que declaras dentro del data, aquí no tienes que hacer un return ni tienes que usar una sintaxis rara, directamente declaras el objeto y dentro cada uno de los props.

```
export default {
  props: {
    title: String
  }
};
```

### Cómo usar los props dentro del componente
Los props los tienes que considerar como una variable más del data, es decir, puedes acceder desde cualquier método o computada a los props con el this.

```
export default {
  props: {
    columns: {
      type: Number,
      default: 4
    },
    rows: {
      type: Number,
      default: 3
    }
  },
  computed: {
    totalCells() {
      return this.columns * this.rows;
    }
  }
};
```

### Cómo pasar los props al componente
Para usar un componente con props tienes que importarlo como ya vimos, y en la etiqueta tienes que pasar los props como un atributo más del html, es decir:

```
<template>
  <div class="home">
    <calendar :rows="6" :columns="5"  />
  </div>
</template>

<script>
// @ es un alias a /src
import Calendar from "@/components/Calendar.vue";

export default {
  components: {
    Calendar
  }
};
</script>
```

### Watchers
Si bien las propiedades computadas son más apropiadas en la mayoría de los casos, hay ocasiones en que es necesario un observador personalizado. Es por eso que Vue proporciona una forma más genérica de reaccionar a los cambios de datos a través de la opción watch. Esto es más útil cuando desea realizar operaciones asíncronas o costosas en respuesta al cambio de datos.


# Vue-example

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```
